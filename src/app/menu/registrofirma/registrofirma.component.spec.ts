import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrofirmaComponent } from './registrofirma.component';

describe('RegistrofirmaComponent', () => {
  let component: RegistrofirmaComponent;
  let fixture: ComponentFixture<RegistrofirmaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrofirmaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrofirmaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
