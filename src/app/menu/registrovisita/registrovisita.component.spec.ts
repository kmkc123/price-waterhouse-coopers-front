import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrovisitaComponent } from './registrovisita.component';

describe('RegistrovisitaComponent', () => {
  let component: RegistrovisitaComponent;
  let fixture: ComponentFixture<RegistrovisitaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrovisitaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrovisitaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
