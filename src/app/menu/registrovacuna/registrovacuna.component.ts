import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import Swal from 'sweetalert2'
import { UsuarioService } from '../services/usuario.service';

@Component({
  selector: 'app-registrovacuna',
  templateUrl: './registrovacuna.component.html',
  styleUrls: ['./registrovacuna.component.css']
})
export class RegistrovacunaComponent implements OnInit {

  constructor(  private router: Router,
    private fb: FormBuilder,
    private usuarioService : UsuarioService) {

      usuarioService.getJSONData().subscribe(data=>{
        console.log("recibe valores del otro componente")
        console.log(data)
      })

     }

  ngOnInit(): void {
  }

  registrovacuna = this.fb.group({
    numDosis : ['', Validators.required],
    tipovacuna : ['', Validators.required],
    especificacion : ['', Validators.required],
    fecVacunacion: ['', Validators.required],
    lote: ['', Validators.required]
  });

  isSubmit : boolean = false;
  mostrarEspecifiqueVacuna : boolean = false;

  tipoVacunaSeleccionado(){

    debugger

    this.mostrarEspecifiqueVacuna = this.registrovacuna.value.tipovacuna == "-1";
    if(this.mostrarEspecifiqueVacuna){
      this.especificacion?.setValidators([Validators.required]);
    }else{
      this.especificacion?.setValidators(null);
    }

    this.especificacion?.updateValueAndValidity();

    console.log(this.registrovacuna.value)
  }
  
  grabar(){

    
    this.isSubmit = true;

    if(this.registrovacuna.valid){

      Swal.fire({
        text: 'Información registrada correctamente',
        confirmButtonColor: '#3085d6',
        icon: 'success',
        confirmButtonText: 'Aceptar'
      })

    }

    console.log(this.registrovacuna.value)

  }

  get numDosis() { return this.registrovacuna.get('numDosis'); }
  get tipovacuna() { return this.registrovacuna.get('tipovacuna'); }
  get especificacion() { return this.registrovacuna.get('especificacion'); }
  get fecVacunacion() { return this.registrovacuna.get('fecVacunacion'); }
  get lote() { return this.registrovacuna.get('lote'); }

}
