import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'app-resportevisita',
  templateUrl: './resportevisita.component.html',
  styleUrls: ['./resportevisita.component.css']
})
export class ResportevisitaComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  fecDesde=null;
  fecHasta=null;

  listaBusqueda:Array<any> = [{"numDoc":"12345678", "trabajador":"Timoteo", "piso":"Piso 1","cubiculo":"C12","asiento":"P1C12D1"},
  {"numDoc":"13568965", "trabajador":"The Backyardigans", "piso":"Piso 2","cubiculo":"C12","asiento":"P1C12D1"}];

  listaMostrar:Array<any> = [];

  buscar(){

    console.log("desde:"+this.fecDesde+" hasta:"+this.fecHasta)

    this.listaMostrar = this.listaBusqueda;

  }

  descargar(data: any){
    console.log(data)
  }
}
