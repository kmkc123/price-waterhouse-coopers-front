import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResportevisitaComponent } from './resportevisita.component';

describe('ResportevisitaComponent', () => {
  let component: ResportevisitaComponent;
  let fixture: ComponentFixture<ResportevisitaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResportevisitaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResportevisitaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
