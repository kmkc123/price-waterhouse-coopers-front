import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-resportevacunados',
  templateUrl: './resportevacunados.component.html',
  styleUrls: ['./resportevacunados.component.css']
})
export class ResportevacunadosComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  
  numDoc = null;
  nombres = null;
  apellidos = null;
  linea = null;

  listaMostrar:Array<any> = [];

  persona: any = null;

  buscar(){


    this.listaMostrar.push({
      "numDocumento":"12345678",
      "nombre":"Jhon",
      "Apellido":"Silvester Stalone",
      "numDosis":"5",
      "tipoVacuna":"Pfizer",
      "fecvacunacion":"04/04/2022",
      "lote":"10",
      "estavacunado":"SI",
      "linea":"Administracion"
    });

    this.listaMostrar.push({
        "numDocumento":"12564875",
        "nombre":"chuck norris",
        "Apellido":"Ray Norris",
        "numDosis":"5",
        "tipoVacuna":"Pfizer",
        "fecvacunacion":"05/04/2022",
        "lote":"10",
        "estavacunado":"SI",
        "linea":"Administracion"
      });

  }

  mostrar(data: any){
    this.persona = data;
    console.log(data)
  }

  descargar(data: any){
    console.log(data)
  }

}
