import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResportevacunadosComponent } from './resportevacunados.component';

describe('ResportevacunadosComponent', () => {
  let component: ResportevacunadosComponent;
  let fixture: ComponentFixture<ResportevacunadosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResportevacunadosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResportevacunadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
