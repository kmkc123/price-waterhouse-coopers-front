import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { UsuarioService } from '../services/usuario.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-registrousuario',
  templateUrl: './registrousuario.component.html',
  styleUrls: ['./registrousuario.component.css']
})
export class RegistrousuarioComponent implements OnInit {

  constructor(  private router: Router,
    private fb: FormBuilder ,
    private usuarioService : UsuarioService) {
     }

  ngOnInit(): void {
  }

  registroUsuario = this.fb.group({
    numDocumento : ['', Validators.required],
    nombre : ['', Validators.required],
    apellido: ['', Validators.required],
    fecNacimiento: ['', Validators.required],
    direccion: ['', Validators.required],
    vacunado : ['', Validators.required]
  });

  isSubmit : boolean = false;

  grabar(){

    
    this.isSubmit = true;

    if(this.registroUsuario.valid){

      let estaVacunado = this.registroUsuario.value.vacunado == "1";

      Swal.fire({
        text: 'Usuario registrado correctamente',
        confirmButtonColor: '#3085d6',
        icon: 'success',
        confirmButtonText: 'Aceptar'
      }).then((result) => {
        if (result.isConfirmed && !estaVacunado) {
          Swal.fire({
            text: 'Usted no esta autorizado para poder agendar una visita',
            confirmButtonColor: '#3085d6',
            icon: 'error',
            confirmButtonText: 'Aceptar'
          })
        }else{
          this.usuarioService.setJSONData(this.registroUsuario.value)
          this.router.navigate(['/dashboard/registro-vacuna']);
        }
      })

    }

    console.log(this.registroUsuario.value)

  }

  get numDocumento() { return this.registroUsuario.get('numDocumento'); }
  get nombre() { return this.registroUsuario.get('nombre'); }
  get apellido() { return this.registroUsuario.get('apellido'); }
  get fecNacimiento() { return this.registroUsuario.get('fecNacimiento'); }
  get direccion() { return this.registroUsuario.get('direccion'); }
  get vacunado() { return this.registroUsuario.get('vacunado'); }


}
