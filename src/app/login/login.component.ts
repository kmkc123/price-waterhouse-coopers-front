import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(  private router: Router,
                private fb: FormBuilder ) { }

  ngOnInit(): void {
    
  }

  loginForm = this.fb.group({
    usuario : ['', Validators.required],
    clave: ['', Validators.required]
  });


  isSubmit : boolean = false;

  signIn(){

    this.isSubmit = true;

    if(this.loginForm.valid){
      if(this.usuario?.value == "web" && this.clave?.value == "web"){
        this.router.navigate(['/dashboard']);
      }else{
        Swal.fire({
          text: 'usuario/clave incorrecto!',
          confirmButtonColor: '#3085d6',
          icon: 'error',
          confirmButtonText: 'Aceptar'
        })
      }
    }
  }

  get usuario() { return this.loginForm.get('usuario'); }
  get clave() { return this.loginForm.get('clave'); }


}
